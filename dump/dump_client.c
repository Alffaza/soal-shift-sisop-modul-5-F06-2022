#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#define PORT 8080
  
bool cek_Close(int readvalue, int sock)
{
    if(readvalue != 0)
    {
        return 0;
    }
    close(sock);
    return 1;
}

int main(int argc, char const *argv[]) 
{    
    if(getuid())
    {
        if(argc < 6)
        {
            printf("error syntax\n");
            return -1;
        }
        else
        {
            if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p"))
            {
                return -1;
            }
        }
    }
    else
    {
        if(argc < 2)
        {
            printf("error syntax\n");
            return -1;
        }
    }
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    int sock = 0;
    int readvalue;
    

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Gagal membuat socket \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\nAddress invalid / Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nGagal mengkoneksi\n");
        return -1;
    }

    char type[1024];
    if(getuid())
    {
        strcpy(type,argv[2]);
        strcat(type," ");
        strcat(type,argv[4]);
        strcat(type," dump ");
        send(sock , type , strlen(type) , 0 );
    }
    else
    {
        strcpy(type,"root dump ");
        send(sock , type , strlen(type) , 0 );
    }
    char login_status[750] = {0};
    int status = recv( sock , login_status, 750,0);
    if(!strcmp(login_status,"authentication fgagal"))
    {
        close(sock);
        return -1;
    }

    if(getuid())
    {
        char cmd[750] = {0};
        char rec[750] = {0};
        sprintf(cmd,"USE %s",argv[5]);
        send(sock , cmd , strlen(cmd) , 0 );
        int status = recv( sock , rec, 750,0);
        bzero(cmd,sizeof(cmd));
        strcpy(cmd,"ok");
        send(sock , cmd , strlen(cmd) , 0 );
        if(!strncmp(rec,"database changed to",19))
        {
            do
            {
                bzero(rec,sizeof(rec));
                int status = recv( sock , rec, 750,0);
                if(strcmp(rec,"DONE"))
                {
                    printf("%s\n",rec);
                    bzero(cmd,sizeof(cmd));
                    strcpy(cmd,"ok");
                    send(sock , cmd , strlen(cmd) , 0 );
                }
            }
            while(strcmp(rec,"DONE"));
            
            close(sock);
            return 0;
        }
    }
    else
    {
        char cmd[750] = {0};
        char rec[750] = {0};
        sprintf(cmd,"USE %s",argv[1]);
        send(sock , cmd , strlen(cmd) , 0 );
        int status = recv( sock , rec, 750,0);
        bzero(cmd,sizeof(cmd));
        strcpy(cmd,"ok");
        send(sock , cmd , strlen(cmd) , 0 );
        if(!strncmp(rec,"database changed to",19))
        {
            do{
                bzero(rec,sizeof(rec));
                int status = recv( sock , rec, 750,0);
                if(strcmp(rec,"DONE"))
                {
                    printf("%s\n",rec);
                    bzero(cmd,sizeof(cmd));
                    strcpy(cmd,"ok");
                    send(sock , cmd , strlen(cmd) , 0 );
                }
            }
            while(strcmp(rec,"DONE"));
            
            close(sock);
            return 0;
        }
    }   
    return 0;
}