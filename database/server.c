#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#define PORT 8080
#define CLIENTS 1000
pthread_t tid[CLIENTS];

char *db_path = ".";
char *user_tabel = "./databases/administrator/user.txt";
char *tabel_permission = "./databases/administrator/permission.txt";

void make_user(){
    pid_t child;
    pid_t childSID;
    child = fork();

    if(child < 0) 
    {
        exit(EXIT_FAILURE);
    }
    else if(child > 0) 
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);
    childSID = setsid();

    if(childSID < 0) 
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

bool cek_Close( int *new_socket, int flag){
    
    if(flag == 0)
    {
        close(*new_socket);
        return 1;
    }


    return 0;
}

bool cek_Database(char *database){
    FILE *filein;
    filein = fopen(tabel_permission,"r");


    char temp_database[750],temp_user[750];

    if(filein)
    {

        while(fscanf(filein,"%s %s",temp_database,temp_user) != EOF)
        {

            if(!strcmp(temp_database,database))
            {
                return 1;
            }

        }
        
    }

    return 0;
}

bool cek_User(char *user){
    FILE *filein;
    filein = fopen(user_tabel,"r");


    char tempuser[750],temppass[750];

    if(filein)
    {
        while(fscanf(filein,"%s %s",tempuser,temppass) != EOF)
        {
            if(!strcmp(tempuser,user))
            {
                return 1;
            }
        }
    }
    return 0;
}

void perintah(int *new_socket, char *now_db){
    DIR *curdir;
    struct dirent *de;
    char fpath[750] = {0};
    sprintf(fpath,"%s/databases/%s",db_path,now_db);

    curdir = opendir(fpath);

    if (curdir != NULL)
    {
        while ((de = readdir (curdir))) {
          if(!strncmp(de->d_name, "struktur_", 9))
          {
              
              char tabel_nama[750] = {0};
              char tmp[750] = {0};

              strcpy(tmp,de->d_name);

              char *token = strtok(tmp, "_");
              token = strtok(NULL, "_");
              strcpy(tabel_nama, token);

              FILE *filein;
              char path[750] = {0};
              sprintf(path,"%s/databases/%s/%s",db_path,now_db,de->d_name);

              char kolom[100][750] = {0};

              int n = 0;

              filein = fopen(path,"r");

              if(filein)
              {
                char tmp2[750];
                while((fscanf(filein,"%[^\n]%*c",tmp2)) != EOF)
                {
                strcpy(kolom[n++],tmp2);
                }
                fclose(filein);

                int flag;
                char command[750] = {0};
                char buff[1024] = {0};
                char tabel[750] = {0};
                
                strcpy(tabel,tabel_nama);
                char *token_tabel = strtok(tabel,".");
                sprintf(command,"DROP TABLE %s;",token_tabel);
                send(*new_socket , command , strlen(command) , 0 );
             flag = recv( *new_socket , buff, 1024, 0);
            
                bzero(command,sizeof(command));
                sprintf(command,"CREATE TABLE %s (",token_tabel);
                for(int i = 0; i < n; i++)
                {
                    strcat(command,kolom[i]);
                    strcat(command,",");
                }
                command[strlen(command)-1] = ')';
                strcat(command,";");

                send(*new_socket , command , strlen(command) , 0 );
             flag = recv( *new_socket , buff, 1024, 0);

                bzero(path,sizeof(path));
                sprintf(path,"%s/databases/%s/%s",db_path,now_db,tabel_nama);

                filein = fopen(path,"r");
                if(filein)
                {

                    while((fscanf(filein,"%[^\n]%*c",tmp2)) != EOF)
                    {

                        bzero(command,sizeof(command));
                        sprintf(command,"INSERT INTO %s VALUES (%s);",token_tabel,tmp2);
                        send(*new_socket , command , strlen(command) , 0 );

                     flag = recv( *new_socket , buff, 1024, 0);
                    }

                    fclose(filein);
                }
            }
        }
    }
    char command[750] = {0};
    bzero(command,sizeof(command));
    strcpy(command,"DONE!!!");
    send(*new_socket , command , strlen(command) , 0 );

      (void) closedir (curdir);
    } 
    else perror ("Couldn't open the directory");
}

void Log_append(char *Login_user, char *command){
    FILE *fout;

    char fpath[750]={0};
    sprintf(fpath,"%s/dblog.log",db_path);
    fout = fopen(fpath,"a");

    char now[100] = {0};
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);

	strftime(now,100,"%Y-%m-%d %H:%M:%S",tm);
    fprintf(fout, "%s:%s:%s\n",now, Login_user, command);
	fclose(fout);
}

char* cut(char *text){
    int index = 0;
    while(text[index] == ' ' || text[index] == '\t')
    {
        index++;
    }
    char *temp = strchr(text,text[index]);
    return temp;
}

void file(char path[750],char to[750]){
    FILE* ptr = fopen(path,"a");
    fprintf(ptr,"%s\n",to);
    fclose(ptr);
}

int Create_User(char *buffer,char *ptr){

    char temp_buffer[1024] = {0};
    strcpy(temp_buffer,buffer);
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    char str[10][750];
    int j=0;

    if(strcmp(ptr,"root"))
    {
        return 0;
    }

    while (token != NULL) 
    {
        strcpy(str[j++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp("IDENTIFIED",str[3]) || strcmp("BY",str[4]) || buffer[strlen(buffer)-1] != ';')
    {
        return -1;
    }

    if(j < 6)
    {
        return -1;
    }

    if(!cek_User(str[2]) && strcmp(str[2],"root") != 0)
    {
        char tmp[750];
        strcpy(tmp,str[2]);
        strcat(tmp," ");
        strcat(tmp,str[5]);
        file(user_tabel,tmp);
        return 1;
    }
    else 
    {
        return -2;
    }
}

int cek_status(char *buffer, char *ptr, char *Login_user, char *now_db){
    char temp_buffer[1024] = {0}, database[100] = {0};
    strcpy(temp_buffer,buffer);
    
    
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    token = strtok(NULL," ");
    
    if(token != NULL)
    {
        strcpy(database,token);
    }
    else
    {
        return -1;
    }


    FILE *filein;
    filein = fopen(tabel_permission,"r");

    char temp_database[750],temp_user[750];
    bool find = false;
    if(filein)
    {
        while(fscanf(filein,"%s %s",temp_database,temp_user) != EOF){
            if(!strcmp(temp_database,database)){
                find = true;
                if(!strcmp(temp_user,Login_user) || !strncmp(ptr,"root",4)){
                    fclose(filein);
                    strcpy(now_db, database);
                    return 1;
                }
            }
        }
    }
    if(!find)
    {
        return -2;
    }
    return 0;
}

int Create_DB(char *buffer, char *ptr, char *Login_user){
    char temp_buffer[1024] = {0}, db[100] = {0};
    strcpy(temp_buffer,buffer);
    
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    if(token != NULL)
    {
        strcpy(db,token);
    }
    else
    {
        return -1;
    }

    char fpath[750] = {0};
    sprintf(fpath,"%s/databases/%s",db_path,db);
    
    if(mkdir(fpath,0777) == 0)
    {
        char history[750] = {0};

        sprintf(history,"%s %s", db, Login_user);

        file(tabel_permission, history);      
        return 1;
    }
    else
    {
        return 0;
    }

}

int grand_permis(char *buffer, char *ptr){
    int j=0;
    char temp_buffer[1024] = {0};
    strcpy(temp_buffer,buffer);
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    char str[10][750];
    
    if(strcmp(ptr,"root"))
    {
        return 0;
    }

    while (token != NULL) 
    {
        strcpy(str[j++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp("INTO",str[3]) || buffer[strlen(buffer)-1] != ';')
    {
        return -1;
    }

    char temp[750] = {0};
    if(j < 5)
    {
        return -1;
    }
    if(cek_Database(str[2]))
    {
        if(cek_User(str[4]))
        {
            sprintf(temp, "%s %s", str[2], str[4]);
            
            file(tabel_permission,temp);
            return 1;
        }
        else
        {
            return -3;
        }
    }
    else
    {
        return -2;
    }
}

int Create_Tabel(char *buffer,char *now_db){
    if(strlen(now_db) == 0)
    {
        return -2;
    }
    char tabel[100] = {0};
    char temp_buffer[1024] = {0};
    strcpy(temp_buffer,buffer);
    
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    if(token != NULL)
    {
        strcpy(tabel,token);
    }
    else
    {
        return -1;
    }

    char *temp;
    temp = strchr(buffer,'(');
    if(!temp)
    {
        return -1;
    }
    else
    {
        temp = temp + 1;
    }

    char kolom[750] = {0};
    strcpy(kolom,temp);

    char *token1 = strtok(kolom,";");
    token1 = strtok(kolom,")");

    char split[100][100] = {0};
    int j = 0;
    token1 = strtok(kolom,",");
    while(token1!=NULL)
    {
        strcpy(split[j++],cut(token1));
        token1 = strtok(NULL,",");
    }

    //cek kelengkapan nama kolom dan tipe kolom
    for(int i = 0; i < j; i++)
    {
        char temp2[750] = {0},nama_kolom[100] = {0}, tipe_kolom[20] = {0};
        strcpy(temp2,split[i]);
        char *token2 = strtok(temp2," ");
        strcpy(nama_kolom,token2);
        token2 = strtok(NULL," ");
        if(token2)
        {
            strcpy(tipe_kolom,token2);
            if(strcmp(tipe_kolom,"int") && strcmp(tipe_kolom,"string"))
            {
                return -4;
            }
        }
        else
        {
            return -3;
        }
    }

    char fpath[750] = {0};
    sprintf(fpath,"%s/databases/%s/%s.txt",db_path,now_db,tabel);
    FILE *open;
    if(!(open = fopen(fpath,"r")))
    {
        open = fopen(fpath,"w");
        char struktur_table[750] = {0};
        sprintf(struktur_table,"%s/databases/%s/struktur_%s.txt",db_path,now_db,tabel);

        for(int i = 0; i < j; i++)
        {
            file(struktur_table,split[i]);
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

void *Delete_Folder(void *arg){
    pid_t child;
    child = fork();
    if(child == 0)
    {
        char *fpath = (char *) arg;
        char *argv[] = {"rm","-rf",fpath, NULL};
        execv("/bin/rm",argv);
    }
}

int Drop_DB(char *buffer, char *tipe, char *Login_user, char *now_db){
    char database[100] = {0};
    char temp_buffer[1024] = {0};
    strcpy(temp_buffer,buffer);
    
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");
    
    if(token != NULL)
    {
        strcpy(database,token);
    }
    else
    {
        return -1;
    }

    FILE *filein;
    FILE *fileout;
    filein = fopen(tabel_permission,"r");

    char temp_database[750];
    char temp_user[750];
    bool find = false;
    bool point = false;
    if(filein)
    {
        while(fscanf(filein,"%s %s",temp_database,temp_user) != EOF)
        {
            if(!strcmp(temp_database,database))
            {
                find = true;
                if(!strcmp(temp_user,Login_user))
                {
                    fclose(filein);
                    point = true;
                    break;
                }
            }
        }

        if(point || (find && !strcmp(Login_user,"root")))
        {              
            char fpath[750] = {0};
            sprintf(fpath,"%s/databases/%s",db_path,database);
            pthread_t thread1;
            int tread = pthread_create(&thread1,NULL,Delete_Folder,fpath);;
            pthread_join(thread1,NULL);
            
            filein = fopen(tabel_permission,"r");
            char temp_permission[750] = {0};
            sprintf(temp_permission,"%s/databases/administrator/temp.txt",db_path);
            fileout = fopen(temp_permission,"w");
            while(fscanf(filein,"%s %s",temp_database,temp_user) != EOF)
            {
                if(strcmp(temp_database,database))
                {
                    char history[750] = {0};
                    fprintf(fileout, "%s %s\n", temp_database, temp_user);
                }
            }
            fclose(fileout);
            remove(tabel_permission);
            rename(temp_permission,tabel_permission);

            if(!strcmp(now_db,database))
            {
                bzero(now_db,sizeof(now_db));
            }
            return 1;
        }        
    }
    if(!find)
    {
        return -2;
    }
    return 0;
}

int valid(char *buffer){
    char tmp[750];
    strcpy(tmp,buffer);
    char a = '\'';

    if(tmp[0]== a && tmp[strlen(tmp)-1] == a)
    {
        return 1;
    }
    for(int i = 0; i < strlen(tmp); i++)
    {
        if(tmp[i] < '0' || tmp[i] > '9')
        {
            return 0;
        }
    }
    return 2;
}

int insert(char *buffer, char *now_db){

    if(!strlen(now_db)){
        return -1;
    }    

    char tmp[750];
    strcpy(tmp,buffer);

    char *token = strtok(tmp,"(");
    token = strtok(NULL,"(");
    token = strtok(token,")");
    token = strtok(token,",");

    char data[100][750];

    int i = 0;

    while(token!=NULL)
    {
        strcpy(data[i],cut(token));
        i++;
        token = strtok(NULL,",");
    }

    FILE *filein,*fileout;

    strcpy(tmp,buffer);
    token = strtok(tmp," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char open[750] = {0};
    char append[750] = {0};
    sprintf(open,"%s/databases/%s/struktur_%s.txt",db_path,now_db,token);
    filein = fopen(open,"r");

   
    char data_tipe[100][750];
    char tmp_tipe[750];
    char a[750];
    int x = 0;

    if(filein)
    {
        while(fscanf(filein,"%s %s",a,tmp_tipe) != EOF)
        {
            strcpy(data_tipe[x],tmp_tipe);
            x++;
        }
    }
    else
    {
        return -2;
    }

    sprintf(append,"%s/databases/%s/%s.txt",db_path,now_db,token);
    fileout = fopen(append,"a");

    if(x != i)
    {
        fclose(filein);
        fclose(fileout);
        return -3;
    }

    for(int j=0;j<i;j++)
    {
        int val = valid(data[j]);
        if(val == 1 && !strcmp(data_tipe[j],"string")) ;
        else if (val == 2 && !strcmp(data_tipe[j],"int")) ;
        else{
            fclose(filein);
            fclose(fileout);
            return -4;
        }  
    }
    for(int j=0;j<i-1;j++)
    {
        fprintf(fileout,"%s,",data[j]);
    }
    fprintf(fileout,"%s\n",data[i-1]);
    fclose(filein);
    fclose(fileout);

    return 1;
}

int Drop_tabel(char *buffer, char *now_db){

    if(!strlen(now_db))
    {
        return -2;
    }

    FILE *filein;

    char tmp[750];
    char *token;
    strcpy(tmp,buffer);
    token = strtok(tmp,";");
    token = strtok(token," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char buka[750] = {0};
    char tambah[750]={0};
    sprintf(buka,"%s/databases/%s/struktur_%s.txt",db_path,now_db,token);
    filein = fopen(buka,"r");

    sprintf(tambah,"%s/databases/%s/%s.txt",db_path,now_db,token);

    if(filein)
    {
        fclose(filein);
        remove(buka);
        remove(tambah);

        return 1;
    }
    else
    {
        return -1;
    }
}

int Drop_kolom(char *buffer, char *now_db){

    if(!strlen(now_db))
    {
        return -2;
    }

    char str[10][750];
    char temp[90000];
    strcpy(temp,buffer);
    int x = 0;
    char *token;
    token = strtok(temp,";");
    token = strtok(token," ");
    while (token != NULL) {
        strcpy(str[x++],token);
        token = strtok(NULL, " ");
    }

    if(x != 5)
    {
        return -3;
    }

    FILE *struktur1;
    FILE *struktur2;
    FILE *tabel1;
    FILE *tabel2;

    char buka[750]={0};
    char tambah[750]={0};
    char a[750]={0};
    char b[750]={0};
    sprintf(buka,"%s/databases/%s/struktur_%s",db_path,now_db,str[4]);
    strcpy(a,buka);
    strcat(a,"2.txt");
    strcat(buka,".txt");

    struktur1 = fopen(buka,"r");
   
    int i = 0;
    int j = 0;
    char data_tipe[1000], nama[1000];

    if(struktur1)
    {
        struktur2 = fopen(a,"w");
        while(fscanf(struktur1,"%s %s",nama,data_tipe) != EOF)
        {
            if(strcmp(str[2],nama))
            {
                fprintf(struktur2,"%s %s\n",nama,data_tipe);
            }
            else i = j;
            j++;
        }
        fclose(struktur1);
        fclose(struktur2);
    }
    else
    {
        return -1;
    }

    sprintf(tambah,"%s/databases/%s/%s",db_path,now_db,str[4]);
    strcpy(b,tambah);
    strcat(b,"2.txt");
    strcat(tambah,".txt");

    tabel1 = fopen(tambah,"r");
    tabel2 = fopen(b,"w");

    char puts[750];
    while(fgets(puts,750,tabel1))
    {
        token = strtok(puts,",");
        int j = 0;
        char new_token[750];
        strcpy(new_token,"");
        while(token!=NULL)
        {
            if(j!=i)
            {
                strcat(new_token,token);
                strcat(new_token,",");
            }
            token = strtok(NULL,",");
            j++;
        }
        new_token[strlen(new_token)-1] = 0;
        fprintf(tabel2,"%s",new_token);
        if(new_token[strlen(new_token)-1] != '\n')
        {
            fprintf(tabel2,"\n");
        }
    }
    fclose(tabel1);
    fclose(tabel2);
    remove(buka);
    rename(a,buka);
    remove(tambah);
    rename(b,tambah);

    return 1;
}

int login(char* tipe, char *Login_user){
   
    FILE *filein;
    filein = fopen(user_tabel,"r");
    printf("%s\n",tipe);
    char username[750];
    char password[750];

    char* token = strtok(tipe, " ");
    strcpy(username,token);
    token = strtok(NULL, " ");
    strcpy(password,token);

    char temp_username[750];
    char temppass[750];
    if(filein)
    {
        while(fscanf(filein,"%s %s",temp_username,temppass) != EOF)
        {
            if(!strcmp(temp_username,username) && !strcmp(temppass,password))
            {
                fclose(filein);
                strcpy(Login_user,username);
                return 1;
            }
        }
        fclose(filein);
    }

    return 0;
    
}

int Delete_data(char *buffer,char *now_db){

    if(!strlen(now_db))
    {
        return -1;
    }

    char temp_buffer[1024] = {0};
    strcpy(temp_buffer,buffer);
   
    char* token = strtok(temp_buffer, ";");
    token = strtok(temp_buffer," ");
    char str[10][750];
    int i=0;

    while (token != NULL){
        strcpy(str[i++],token);
        token = strtok(NULL, " ");
    }

    if(i == 3){
        char path[10000];
        sprintf(path,"%s/databases/%s/%s.txt",db_path,now_db,str[2]);
        FILE *file1,*file2;
        file1 = fopen(path,"r");
        if(file1)
        {
            fclose(file1);
            file2 = fopen(path,"w");
            fprintf(file2,"");
            fclose(file2);
        }
        else
        {
            return -3;
        }
        return 1;
    }
    else if(i == 5)
    {
        if(strcmp(str[3],"WHERE"))
        {
            return -2;
        }

        char tabel_nama[750];
        char cmp[1000];

        token = strtok(str[4],"=");
        strcpy(tabel_nama,token);
        token = strtok(NULL,"=");
        strcpy(cmp,token);

        char path[10000];
        char struk[10000];
        char n_path[10000];

        sprintf(struk,"%s/databases/%s/struktur_%s.txt",db_path,now_db,str[2]);
        FILE *struktur1;
        struktur1 = fopen(struk,"r");

        int i = -1;
        int k = 0;
        char r[1000],tabel[750];
        if(struktur1){
            while(fscanf(struktur1,"%s %s",tabel,r) != EOF)
            {
                if(!strcmp(tabel_nama,tabel))
                {
                    i = k;
                }
                k++;
            }
            if(i == -1)
            {
                fclose(struktur1);
                return -4;
            }
            fclose(struktur1);
        }
        else
        {
            return -3;
        }

        sprintf(path,"%s/databases/%s/%s.txt",db_path,now_db,str[2]);
        sprintf(n_path,"%s/databases/%s/%s2.txt",db_path,now_db,str[2]);
        FILE *file1,*file2;
        file1 = fopen(path,"r");
        file2 = fopen(n_path,"w");
        char puts[750];
        if(file1){
            while(fgets(puts,750,file1)){
                token = strtok(puts,",");
                int j = 0;
                char new_token[750];
                strcpy(new_token,"");
                int flag = 0;
                while(token!=NULL){
                    strcat(new_token,token);
                    strcat(new_token,",");
                    if(j==i){
                        if(!strcmp(cmp,token)){
                            flag = 1;
                        }
                    }
                    token = strtok(NULL,",");
                    j++;
                }
                if(!flag)
                {
                    new_token[strlen(new_token)-1] = 0;
                    fprintf(file2,"%s",new_token);
                    if(new_token[strlen(new_token)-1] != '\n'){
                        fprintf(file2,"\n");
                    }
                }
            }
            fclose(file1);
            fclose(file2);
            remove(path);
            rename(n_path,path);
            return 1;
        }
        else
        {
            return -3;
        }
    }
    else
    {
        return -2;
    }
}

int Update_DB(char *buffer,char *now_db){
    
    if(!strlen(now_db))
    {
        return -1;
    }

    char buffer_tmp[1024] = {0};
    strcpy(buffer_tmp,buffer);
   
    char* token = strtok(buffer_tmp, ";");
    char str[10][750];
    token = strtok(buffer_tmp," ");
    int j = 0;

    while (token != NULL) 
    {
        strcpy(str[j++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp(str[2],"SET"))
    {
        return -2;
    }

    char nama_tabel[750]; 
    char cmp[750];

    token = strtok(str[3],"=");
    strcpy(nama_tabel,token);
    token = strtok(NULL,"=");
    strcpy(cmp,token);

    char path1[10000];
    char struc[10000];
    char path2[10000];

    sprintf(struc,"%s/databases/%s/struktur_%s.txt",db_path,now_db,str[1]);
    FILE *struktur1;
    struktur1 = fopen(struc,"r");

    j = -1;
    int z = 0;
    char r[1000];
    char tabel[1000];
    if(struktur1){
        while(fscanf(struktur1,"%s %s",tabel,r) != EOF)
        {
            if(!strcmp(nama_tabel,tabel))
            {
                j = z;
            }
            z++;
        }
        if(j == -1)
        {
            fclose(struktur1);
            return -4;
        }
        fclose(struktur1);
    }
    else
    {
        return -3;
    }

    int x = -1;
    char cmp2[750];
    char temp[750];
    char cmp3[750];


    if(strlen(str[4]))
    {
        token = strtok(str[5],"=");
        strcpy(cmp2,token);
        token = strtok(NULL,"=");
        strcpy(cmp3,token);
        z = 0;
        if(!strcmp(str[4],"WHERE")){
            struktur1 = fopen(struc,"r");
            while(fscanf(struktur1,"%s %s",temp,r) != EOF)
            {
                if(!strcmp(cmp2,temp))
                {
                    x = z;
                }
                z++;
            }
            if(x == -1)
            {
                fclose(struktur1);
                return -4;
            }
            fclose(struktur1);
        }
    }
    // printf("%d %d\n", x, z);
    sprintf(path1, "%s/databases/%s/%s.txt", db_path, now_db, str[1]);
    sprintf(path2,"%s/databases/%s/%s2.txt",db_path,now_db,str[1]);
    FILE *file1,*file2;
    file1 = fopen(path1,"r");
    file2 = fopen(path2,"w");
    char put[750];
    char old[750];
    if(file1){
        while(fgets(put,750,file1))
        {
            strcpy(old,put);
            token = strtok(put,",");
            int t = 0;
            char new_token[750];
            strcpy(new_token,"");
            int p = 0;
            while(token!=NULL)
            {
                if(t == x)
                {
                    if(!strcmp(cmp3,token)){
                        p = 1;
                    }
                }
                if(t == z)
                {
                    strcat(new_token,cmp);
                    strcat(new_token,",");
                }
                else
                {
                    strcat(new_token,token);
                    strcat(new_token,",");
                }
                token = strtok(NULL,",");
                t++;
            }
            if(p == 1 || x == -1)
            {
                new_token[strlen(new_token)-1] = 0;
                fprintf(file2,"%s",new_token);
                if(new_token[strlen(new_token)-1] != '\n')
                {
                    fprintf(file2,"\n");
                }
            }
            else
            {
                old[strlen(new_token)-1] = 0;
                fprintf(file2,"%s",old);
                if(old[strlen(old)-1] != '\n')
                {
                    fprintf(file2,"\n");
                }
            }
        }
        fclose(file1);
        fclose(file2);
        remove(path1);
        rename(path2,path1);
        return 1;
    }
}

int Select_tabel(char *buffer, char *now_db, int *new_socket){
    if(!strlen(now_db))
    {
        return -1;
    }

    char buffer_tmp[1024] = {0};
    strcpy(buffer_tmp,buffer);
   
    char* token = strtok(buffer_tmp, ";");
    token = strtok(buffer_tmp," ");
    char str[10][750];
    int x = 0;

    while (token != NULL)
    {
        strcpy(str[x++],token);
        token = strtok(NULL, " ");
    }

    int all;
    int y = -1;
    int z;
    int cases;
    int sor[20];
    char cmp1[750];
    char temp[750];
    char cmp2[750];
    char r[750];
    char path1[10000];
    char struc[10000];
    char cmp3[20][750];

    FILE *struktur1;

    if(!strcmp(str[1],"*"))
    {
        sprintf(struc,"%s/databases/%s/struktur_%s.txt",db_path,now_db,str[3]);

        struktur1 = fopen(struc,"r");

        if(!struktur1)
        {
            return -3;
        }

        fclose(struktur1);

        cases = 2;
        all = -1;
        
        if(strcmp(str[2],"FROM"))
        {
            return -2;
        }
        if(x >= 5 && !strcmp(str[4],"WHERE"))
        {
            token = strtok(str[5],"=");
            strcpy(cmp1,token);
            token = strtok(NULL,"=");
            strcpy(cmp2,token);

            z = 0;

            if(!strcmp(str[4],"WHERE"))
            {
                struktur1 = fopen(struc,"r");
                while(fscanf(struktur1,"%s %s",temp,r) != EOF)
                {
                    if(!strcmp(cmp1,temp))
                    {
                        y = z;
                    }
                    z++;
                }
                if(y == -1)
                {
                    fclose(struktur1);
                    return -4;
                }
                fclose(struktur1);
            }
        }
    }

    else
    {
        cases = 2;
        while(cases <= x && strcmp(str[cases++],"FROM"))
        {    
        }

        if(cases > x)
        {
            return -2;
        }
        sprintf(struc,"%s/databases/%s/struktur_%s.txt",db_path,now_db,str[cases]);
        struktur1 = fopen(struc,"r");

        if(!struktur1)
        {
            printf("ka %d %s\n",cases,struc);
            return -3;
        }

        fclose(struktur1);

        all = 0;
        cases = 1;
        
        for(int i = 0; i < 20; i++)
        {
            sor[i] = -1;
        }

        char temp2[750];
        while(cases < x && strcmp(str[cases],"FROM"))
        {
            z = 0;
            struktur1 = fopen(struc,"r");
            while(fscanf(struktur1,"%s %s",temp,r) != EOF)
            {
                strcpy(temp2,temp);
                strcat(temp2,",");
                if(!strcmp(r[cases],temp) || !strcmp(temp2,str[cases]))
                {
                    strcpy(cmp3[all],str[cases]);
                    sor[all] = z;
                    all++;
                }
                z++;
            }
            if(all == 0 || sor[all-1] == -1)
            {
                fclose(struktur1);
                return -4;
            }
            fclose(struktur1);
            cases++;
        }
        if(cases == x)
        {
            return -3;
        }
        if(x >= cases+2 && !strcmp(str[cases+2],"WHERE"))
        {
            token = strtok(str[cases+3],"=");
            strcpy(cmp1,token);
            token = strtok(NULL,"=");
            strcpy(cmp2,token);

            z = 0;

            if(!strcmp(str[cases+2],"WHERE"))
            {
                struktur1 = fopen(struc,"r");
                while(fscanf(struktur1,"%s %s",temp,r) != EOF)
                {
                    if(!strcmp(cmp1,temp))
                    {
                        y = z;
                    }
                    z++;
                }
                if(y == -1)
                {
                    fclose(struktur1);
                    return -4;
                }
                fclose(struktur1);
            }
        }
    }

    sprintf(path1,"%s/databases/%s/%s.txt",db_path,now_db,str[cases+1]);
    FILE *file1;
    file1 = fopen(path1,"r");
    char put[750];
    char old[750];
    char new_token[750];
    char result_msg[750] = "";

    if(file1)
    {
        char acc[1024]={0};
        while(fgets(put,1000,file1))
        {
            if(all == -1)
            {
                if(y == -1)
                {
                    char txt[750] = {0};
                    char buffer2[750] = {0};
                    strcpy(txt,put);
                    strcat(result_msg, txt);
                }
                else
                {
                    char hd[750];
                    strcpy(hd,put);
                    token = strtok(put,",");
                    int j = 0;
                    strcpy(new_token,"");
                    int flag = 0;
                    while(token!=NULL)
                    {
                        if(j == y)
                        {
                            if(!strcmp(cmp2,token))
                            {
                                char txt[750] = {0};
                                char buffer2[750] = {0};
                                strcpy(txt,hd);
                                strcat(result_msg, txt);
                                break;
                            }
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
            }
            else
            {
                char so[750];
                char hd[750];
                strcpy(so,"");
                for(int i = 0; i < all; i++)
                {
                    strcpy(hd,put);
                    token = strtok(put,",");
                    int j = 0;
                    strcpy(new_token,"");
                    int flag = 0;
                    while(token!=NULL)
                    {
                        if(j == sor[i])
                        {
                            strcat(so,token);
                            strcat(so,",");
                            break;
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
                char txt[750] = {0};
                char buffer2[750] = {0};
                so[strlen(so)-1]='\0';
                strcpy(txt,so);
                strcat(result_msg, txt);
            }
        }
        char txt[750] = {0};
        char buffer2[1024] = {0};
        send(*new_socket , result_msg , strlen(result_msg) , 0 );
        printf("%s\n", result_msg);
    }
    else
    {
        return -2;
    }
}

void *Start(void *arg){
    int *new_socket = (int *) arg;
    bool dump = false;
    int indicator;
    char type[1024] = {0};
    char tmptype[1024] = {0};
 indicator = recv( *new_socket , type, 1024, 0);
    char Login_user[100] = {0}; 
    char now_db[100]={0};

    strcpy(tmptype,type);

    if(!strncmp(type,"root",4))
    {
        strcpy(Login_user,"root");
        char status_login[1000] = {0};
        strcpy(status_login,"authentication success");
        send(*new_socket , status_login , strlen(status_login) , 0 );
        char *r = strstr(tmptype,"dump");
        if(r)
        {
            dump = true;
        }
    }
    else
    {
        //cek login
        int in = login(type,Login_user);
        char status_login[1000] = {0};
        
        if(in == 0)
        {
            strcpy(status_login,"authentication failed");
            send(*new_socket , status_login , strlen(status_login) , 0 );
            printf("login gagal\n");
            close(*new_socket);
            return;
        }
        strcpy(status_login,"authentication success");
        send(*new_socket , status_login , strlen(status_login) , 0 );
        printf("berhasil login %s\n",Login_user);

        char *r = strstr(tmptype,"dump");
        if(r)
        {
            dump = true;
        }
    }

    if(dump)
    {
        char buffer[1024] = {0};
        char msg[1024] = {0};
     indicator = recv( *new_socket , buffer, 1024, 0);
        if(!strncmp(buffer,"USE",3))
        {
            Log_append(Login_user, buffer);
            int status = cek_status(buffer,type,Login_user,now_db);
            if(status == -2)
            {
                strcpy(msg,"unknown database");
                close(*new_socket);
            }
            else if(status == -1)
            {
                strcpy(msg,"syntax error");
                close(*new_socket);
            }
            else if(status == 1)
            {
                sprintf(msg,"database changed to %s",now_db);
            }
            else if(status == 0)
            {
                strcpy(msg,"permission denied");
                close(*new_socket);
            }
            send(*new_socket , msg , strlen(msg) , 0 );
         indicator = recv( *new_socket , buffer, 1024, 0);
            //generate command
            perintah(new_socket,now_db);
        }

        close(*new_socket);
        return 0;
    }
    while(1)
    {
        char buffer[1024] = {0};
        char msg[1024] = {0};
        char *hello = "Hello -server";

     indicator = recv( *new_socket , buffer, 1024, 0);
        printf("%s\n", buffer);

        if(cek_Close(new_socket, indicator))
        {
            printf("Koneksi terputus\n");
            break;
        }

        if(!strncmp(buffer,"CREATE USER",11))
        {
            Log_append(Login_user, buffer);
            int status = Create_User(buffer,type);
            if(status == -2){
                strcpy(msg,"user already exist");
            }else if(status == -1){
                strcpy(msg,"syntax error");
            }else if(status == 0){
                strcpy(msg,"permission denied");
            }else if(status == 1){
                strcpy(msg,"create user success");
            }
        }
        else if(!strncmp(buffer,"USE",3))
        {
            Log_append(Login_user, buffer);
            int status = cek_status(buffer,type,Login_user,now_db);
            if(status == -2)
            {
                strcpy(msg,"unknown database");
            }
            else if(status == -1)
            {
                strcpy(msg,"syntax error");
            }
            else if(status == 1)
            {
                sprintf(msg,"database changed to %s",now_db);
            }
            else if(status == 0)
            {
                strcpy(msg,"permission denied");
            }
        }
        else if(!strncmp(buffer,"CREATE DATABASE",15))
        {
            Log_append(Login_user, buffer);
            int status = Create_DB(buffer,type,Login_user);
            if(status == -1)
            {
                strcpy(msg,"syntax error");
            }
            else if(status == 1)
            {
                strcpy(msg,"create success");
            }
            else if(status == 0)
            {
                strcpy(msg,"permission denied");
            }
        }
        else if(!strncmp(buffer,"GRANT PERMISSION",16))
        {
            Log_append(Login_user, buffer);
            int status = grand_permis(buffer,type);
            if(status == -3)
            {
                strcpy(msg,"unknown user");
            }
            else if(status == -2)
            {
                strcpy(msg,"database not exist");
            }
            else if(status == -1)
            {
                strcpy(msg,"syntax error");
            }
            else if(status == 1)
            {
                strcpy(msg,"grant permission success");
            }
            else if(status == 0)
            {
                strcpy(msg,"grant permission denied");
            }
        }
        else if(!strncmp(buffer,"CREATE TABLE",12))
        {
            Log_append(Login_user, buffer);
            int status = Create_Tabel
        (buffer,now_db);
            if(status == -4)
            {
                strcpy(msg,"invalid column type");
            }
            else if(status == -3)
            {
                strcpy(msg,"missing column type");
            }
            else if(status == -2)
            {
                strcpy(msg,"no database used");
            }
            else if(status == -1)
            {
                strcpy(msg,"syntax error");
            }
            else if(status == 1)
            {
                strcpy(msg,"create success");
            }
            else if(status == 0)
            {
                strcpy(msg,"table already exist");
            }
        }
        else if(!strncmp(buffer,"DROP DATABASE",13))
        {
            Log_append(Login_user, buffer);
            int status = Drop_DB(buffer,type,Login_user,now_db);
            if(status == -2)
            {
                strcpy(msg,"unknown database");
            }
            else if(status == -1)
            {
                strcpy(msg,"syntax error");
            }
            else if(status == 1)
            {
                sprintf(msg,"database dropped");
            }
            else if(status == 0)
            {
                strcpy(msg,"permission denied");
            }
        }
        else if(!strncmp(buffer,"INSERT INTO",11))
        {
            Log_append(Login_user, buffer);
            int status = insert(buffer,now_db);
            switch (status) 
            {
                case 1:
                    strcpy(msg,"Insert success");
                    break;
                case -1:
                    strcpy(msg,"no database used");
                    break;
                case -2:
                    strcpy(msg,"table does not exist");
                    break;
                case -3:
                    strcpy(msg,"coloumn count doesnt match");
                    break;
                case -4:
                    strcpy(msg,"invalid input");
                    break;
            }
        }
        else if(!strncmp(buffer,"DROP TABLE",10))
        {
            Log_append(Login_user, buffer);
            int status = Drop_tabel(buffer,now_db);
            switch (status)
            {
                case 1:
                    strcpy(msg,"drop table success");
                    break;
                case -1:
                    strcpy(msg,"table does not exist");
                    break;
                case -2:
                    strcpy(msg,"no database used");
                    break;
            }
        }
        else if(!strncmp(buffer,"DROP COLUMN",11))
        {
            Log_append(Login_user, buffer);
            int status = Drop_kolom(buffer,now_db);
            switch (status)
            {
                case 1:
                    strcpy(msg,"drop column success");
                    break;
                case -1:
                    strcpy(msg,"table does not exist");
                    break;
                case -2:
                    strcpy(msg,"no database used");
                    break;
                case -3:
                    strcpy(msg,"invalid syntax");
                    break;
                default:
                    strcpy(msg,"duh");
                    break;
            }
        }
        else if(!strncmp(buffer,"DELETE FROM",11))
        {
            int status = Delete_data(buffer,now_db);
            switch(status)
            {
                case 1:
                    strcpy(msg,"delete success");
                    break;
                case -1:
                    strcpy(msg,"no database used");
                    break;
                case -2:
                    strcpy(msg,"invalid syntax");
                    break;
                case -3:
                    strcpy(msg,"table does not exist");
                    break;
                case -4:
                    strcpy(msg,"column does not exist");
                    break;
                default:
                    strcpy(msg,"duh");
                    break;
            }
        }
        else if(!strncmp(buffer,"UPDATE",6))
        {
            int status = Update_DB(buffer,now_db);
            switch(status)
            {
                case 1:
                    strcpy(msg,"update success");
                    break;
                case -1:
                    strcpy(msg,"no database used");
                    break;
                case -2:
                    strcpy(msg,"invalid syntax");
                    break;
                case -3:
                    strcpy(msg,"table does not exist");
                    break;
                case -4:
                    strcpy(msg,"column does not exist");
                    break;
                default:
                    strcpy(msg,"duh");
                    break;
            }
        }
        else if(!strncmp(buffer,"SELECT",6))
        {
            int status = Select_tabel(buffer,now_db,new_socket);
            switch(status)
            {
                case -1:
                    strcpy(msg,"no database used");
                    break;
                case -2:
                    strcpy(msg,"invalid syntax");
                    break;
                case -3:
                    strcpy(msg,"table does not exist");
                    break;
                case -4:
                    strcpy(msg,"column does not exist");
                    break;
                default:
                    break;
            }
        }else
        {
            Log_append(Login_user, buffer);
            strcpy(msg,"syntax error");
        }

        //debug user dan database yg digunakan
        if(!strcmp(buffer,"STATUS"))
        {
            sprintf(msg,"Login_user:%s now_db:%s",Login_user,now_db);
        } 

        send(*new_socket , msg , strlen(msg) , 0 );

        
    }
}

int main(int argc, char const *argv[]) {

    char path_admin[1000] = {0};
    char path_db[1000] = {0};
    
    sprintf(path_db,"%s/databases",db_path);
    sprintf(path_admin,"%s/administrator",path_db);
    mkdir(path_admin, 0777);
    mkdir(path_db,0777);
    FILE *f;
    if (!fopen(user_tabel, "r"))
    {
        f = fopen(user_tabel,"w");
        fclose(f);
    }
    if(!fopen(tabel_permission,"r"))
    {
        fopen(tabel_permission,"w");
        fclose(f);
    }
    int server_fd;
    int new_socket[CLIENTS];
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    make_user();
    int counter = 0;
    while(1)
    {
        if ((new_socket[counter] = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
        {
            perror("accept\n");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[counter]),NULL,Start,&new_socket[counter]);
        counter++;
        printf("Client %d terhubung\n", counter);
    }
    return 0;
}