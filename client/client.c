#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <sys/socket.h>
#define PORT 8080

bool cek_Close(int readvalue, int sock)
{
    if(readvalue != 0)
    {
        return 0;
    }
    close(sock);
    return 1;
}

int main(int argc, char const *argv[]) {
    bool input_mode = false;

    if(getuid())
    {
        if(argc < 5)
        {
            printf("argumen tidak cukup\n");
            return -1;
        }
        if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p"))
        {
            printf("error syntax\n");
            return -1;
        }
        if(argc == 7)
        {
            if(!strcmp(argv[5],"-d"))
            {
                input_mode = true;
            }else
            {
                printf("error syntax\n");
                return -1;
            }
        }
    }
    else
    {
        if(argc == 3)
        {
            if(!strcmp(argv[1],"-d"))
            {
                input_mode = true;
            }
            else
            {
                printf("error syntax\n");
                return -1;
            }
        }
    }

    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    int sock = 0;
    int readvalue;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Gagal membuat socket \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\n address Invalid / Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nGagal menyambungkan socket\n");
        return -1;
    }
    char type[1024];
    if(getuid())
    {
        strcpy(type,argv[2]);
        strcat(type," ");
        strcat(type,argv[4]);
        send(sock , type , strlen(type) , 0 );
    }
    else
    {
        strcpy(type,"root");
        send(sock , type , strlen(type) , 0 );
    }
    char login_status[750] = {0};
    int val = recv( sock , login_status, 750,0);
    printf("%s\n",login_status);

    if(!strcmp(login_status,"gagal authentication"))
    {
        close(sock);
        return -1;
    }

    if(getuid())
    {
        if(input_mode){
            char cmd[750] = {0};
            char rec[750] = {0};
            sprintf(cmd,"USE %s",argv[6]);
            send(sock , cmd , strlen(cmd) , 0 );
            int val = recv( sock , rec, 750,0);
            if(!strncmp(rec,"database changed to",19))
            {
                char tmp[750];
                while((fscanf(stdin,"%[^\n]%*c",tmp)) != EOF)
                {
                    send(sock , tmp , strlen(tmp) , 0 );
                    bzero(rec,sizeof(rec));
                    int val = recv( sock , rec, 750,0);
                    printf("%s\n",rec);
                };
                close(sock);
                return 0;
            }
        }
    }
    else
    {
        if(input_mode)
        {
            char cmd[750] = {0}, rec[750] = {0};
            sprintf(cmd,"USE %s",argv[2]);
            send(sock , cmd , strlen(cmd) , 0 );
            int val = recv( sock , rec, 750,0);
            if(!strncmp(rec,"database changed to",19))
            {
                char tmp[750];
                while((fscanf(stdin,"%[^\n]%*c",tmp)) != EOF)
                {
                    send(sock , tmp , strlen(tmp) , 0 );
                    bzero(rec,sizeof(rec));
                    int val = recv( sock , rec, 750,0);
                    printf("%s\n",rec);
                };
                close(sock);
                return 0;
            }
        }
    }
    while(1)
    {
        char msg[750] ={0};
        char buffer[1024] = {0};
        gets(msg);
        send(sock , msg , strlen(msg) , 0 );
        // printf("Hello message sent\n");
        readvalue = recv( sock , buffer, 1024,0);
        if(cek_Close(readvalue, sock))
        {
            break;
        }
        if(!strcmp(buffer,"start"))
        {
            char cmd[750] = {0};
            char rec[750] = {0};
            strcpy(cmd,"ok");
            send(sock , cmd , strlen(cmd) , 0 );
            do
            {
                bzero(rec,sizeof(rec));
                int val = recv( sock , rec, 750,0);
                if(strcmp(rec,"Selesai"))
                {
                    if(rec[strlen(rec)-1] != '\n')
                    {
                        printf("%s\n",rec);
                    }
                    else
                    {
                        printf("%s",rec);
                    }
                    
                    bzero(cmd,sizeof(cmd));
                    strcpy(cmd,"ok");
                    send(sock , cmd , strlen(cmd) , 0 );
                }
            }
            while(strcmp(rec,"Selesai"));
            bzero(cmd,sizeof(cmd));
            strcpy(cmd,"ok");
            send(sock , cmd , strlen(cmd) , 0 );
            int val = recv( sock , buffer, 1024,0);
            printf("%s\n",buffer);
        }
        else
        {
            printf("%s\n",buffer );
        }
    }
    return 0;
}